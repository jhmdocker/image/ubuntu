ARG UBUNTU_VERSION=ND
FROM ubuntu:$UBUNTU_VERSION

ENV TZ=America/Sao_Paulo
ENV DEBIAN_FRONTEND=noninteractive

ADD rootfs /

RUN apt-get update && apt-get install -y wget curl ca-certificates tzdata
