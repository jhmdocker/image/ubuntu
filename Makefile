VERSION=24.04
IMG=registry.gitlab.com/jhmdocker/image/ubuntu

build:
	docker build $(BUILD_OPTS) -t $(IMG) \
		--build-arg UBUNTU_VERSION=$(VERSION) \
		.

push: build
	docker tag $(IMG) $(IMG):$(VERSION)
	docker push $(IMG)
	docker push $(IMG):$(VERSION)
